from django.contrib import admin
from django.apps import apps

user_management = apps.get_app_config('user_management')
for model in user_management.get_models():
    admin.site.register(model)
