from django.shortcuts import render
from django.http import HttpResponseForbidden


# Create your views here.
def auth(method):
    if method != 'GET':
        return False
    return True


def index(request):
    if not auth(request.method): return HttpResponseForbidden()
    return render(request, 'web/index.html')


def contact(request):
    if not auth(request.method): return HttpResponseForbidden()
    return render(request, 'web/contact.html')


def about(request):
    if not auth(request.method): return HttpResponseForbidden()
    return render(request, 'web/about-us.html')
