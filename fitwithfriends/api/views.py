from django.http import HttpResponseForbidden, HttpResponse
from django.shortcuts import render
from django.apps import apps
from datetime import datetime
import json


# Create your views here.
def authenticated(data):
    if data.method != 'POST' or \
        'emailFF' not in data.COOKIES.keys() or \
        'tokenFF' not in data.COOKIES.keys(): return False
    email = data.COOKIES.get('emailFF')
    token = data.COOKIES.get('tokenFF')
    user = apps.get_model('user_management', 'User')
    try:
        user = user.objects.get(email=email)
    except user.DoesNotExist:
        return False
    return token == user.token


def get_quote_otd(request):
    response = HttpResponse()
    response.status_code = 403
    if not authenticated(request): return response
    quote = apps.get_model('user_management', 'Quote')
    qotd = quote.objects.get(day=datetime.date(datetime.now()))
    quote = qotd.quote
    name = qotd.name
    location = qotd.location
    image = qotd.image

    data = json.dumps({
        'quote': quote,
        'name': name,
        'location': location,
        'image': str(image)
    })
    response = HttpResponse(data)
    response.status_code = 200
    return response


def get_step_account(request):
    response = HttpResponse()
    response.status_code = 403
    if not authenticated(request): return response

    user = apps.get_model('user_management', 'User').objects.get(email=request.COOKIES.get('emailFF'))
    steps = list(apps.get_model('user_management', 'Steps').objects.filter(user_id=user))[-7::]

    data = json.dumps({
        7: steps[6].step_count,
        6: steps[5].step_count,
        5: steps[4].step_count,
        4: steps[3].step_count,
        3: steps[2].step_count,
        2: steps[1].step_count,
        1: steps[0].step_count,
    })
    response = HttpResponse(data)
    response.status_code = 200
    return response


def get_calorie_count(request):
    response = HttpResponse()
    response.status_code = 403
    if not authenticated(request): return response

    user = apps.get_model('user_management', 'User').objects.get(email=request.COOKIES.get('emailFF'))
    calories = list(apps.get_model('user_management', 'Calories').objects.filter(user_id=user))[-7::]

    data = json.dumps({
        7: calories[6].calories,
        6: calories[5].calories,
        5: calories[4].calories,
        4: calories[3].calories,
        3: calories[2].calories,
        2: calories[1].calories,
        1: calories[0].calories,
    })

    response = HttpResponse(data)
    response.status_code = 200
    return response


def get_wins(request):
    response = HttpResponse()
    response.status_code = 403
    if not authenticated(request): return response

    user = apps.get_model('user_management', 'User').objects.get(email=request.COOKIES.get('emailFF'))
    calories = list(apps.get_model('user_management', 'Balance').objects.filter(user_id=user))[-30::]

    week1 = calories[24:30]
    week2 = calories[17:23]
    week3 = calories[10:16]
    week4 = calories[0:9]

    wins1 = week1[-1].wins - week1[0].wins
    wins2 = week2[-1].wins - week2[0].wins
    wins3 = week3[-1].wins - week3[0].wins
    wins4 = week4[-1].wins - week4[0].wins

    matches1 = week1[-1].matches - week1[0].matches
    matches2 = week2[-1].matches - week2[0].matches
    matches3 = week3[-1].matches - week3[0].matches
    matches4 = week4[-1].matches - week4[0].matches

    data = json.dumps([
        [
            matches1, matches2, matches3, matches4
        ],
        [
            wins1, wins2, wins3, wins4
        ]
    ])

    response = HttpResponse(data)
    response.status_code = 200
    return response


def get_wallet(request):
    response = HttpResponse()
    response.status_code = 403
    if not authenticated(request): return response

    user = apps.get_model('user_management', 'User').objects.get(email=request.COOKIES.get('emailFF'))
    balance = list(apps.get_model('user_management', 'Balance').objects.filter(user_id=user))[-7::]

    data = json.dumps(
        [
            [
                balance[6].fpoints,balance[5].fpoints,balance[4].fpoints,balance[3].fpoints,balance[2].fpoints,
                balance[1].fpoints,balance[0].fpoints
            ],
            [
                balance[6].currency, balance[5].currency, balance[4].currency, balance[3].currency, balance[2].currency,
                balance[1].currency, balance[0].currency
            ]
        ]
    )

    response = HttpResponse(data)
    response.status_code = 200
    return response


def get_challenges(request):
    response = HttpResponse()
    response.status_code = 403
    if not authenticated(request): return response

    user = apps.get_model('user_management', 'User').objects.get(email=request.COOKIES.get('emailFF'))
    challenges = list(apps.get_model('user_management', 'Challenge').objects.filter(users=user))
    data = json.dumps(challenges)

    return HttpResponse(data)
