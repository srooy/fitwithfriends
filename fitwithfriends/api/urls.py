from django.urls import path
from . import views

urlpatterns = [
    path('/get-quote-otd/', views.get_quote_otd),
    path('/get-step-count/', views.get_step_account),
    path('/get-calorie-count/', views.get_calorie_count),
    path('/get-wins/', views.get_wins),
    path('/get-wallet/', views.get_wallet),
    path('/get-challenges/', views.get_challenges)
]

