# Generated by Django 2.1.7 on 2019-02-19 11:40

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Challenge',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('access_key', models.CharField(default=None, max_length=8)),
                ('type', models.IntegerField()),
                ('competition', models.IntegerField()),
                ('max_players', models.IntegerField()),
                ('start_date', models.DateTimeField()),
                ('end_date', models.DateTimeField()),
                ('price', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fname', models.CharField(max_length=32)),
                ('lname', models.CharField(max_length=48)),
                ('email', models.EmailField(max_length=254)),
                ('password', models.CharField(max_length=128)),
                ('token', models.CharField(max_length=128)),
            ],
        ),
        migrations.AddField(
            model_name='challenge',
            name='users',
            field=models.ManyToManyField(related_name='user', to='user_management.User'),
        ),
    ]
