# Generated by Django 2.1.7 on 2019-02-22 13:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_management', '0003_user_verified'),
    ]

    operations = [
        migrations.CreateModel(
            name='quote',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quote', models.CharField(max_length=368)),
                ('name', models.CharField(max_length=40)),
                ('location', models.CharField(max_length=40)),
                ('day', models.DateTimeField()),
            ],
        ),
    ]
