from django.shortcuts import render
from django.http import HttpResponseForbidden


# Create your views here.
def auth(method):
    if method != 'GET':
        return False
    return True


def register(request):
    if not auth(request.method): return HttpResponseForbidden()
    return render(request, 'user_management/register.html')


def login(request):
    if not auth(request.method): return HttpResponseForbidden()
    return render(request, 'user_management/login.html')


def request_password(request):
    if not auth(request.method): return HttpResponseForbidden()
    return render(request, 'user_management/request-password.html')
