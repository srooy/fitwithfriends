from django.db import models
import hashlib, os, binascii
from django.apps import apps


# Create your models here.
class User(models.Model):
    fname = models.CharField(max_length=32)
    lname = models.CharField(max_length=48)
    email = models.EmailField()
    password = models.CharField(max_length=128)
    gender = models.BooleanField(null=True)  # True = male, False = female
    token = models.CharField(max_length=128)
    verified = models.BooleanField(default=False)
    matches = models.IntegerField()
    wins = models.IntegerField()


    @staticmethod
    def create_password(password):
        """Returns salt of password"""
        salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
        hash = hashlib.pbkdf2_hmac('sha512', password.encode('utf-8'), salt, 100000)
        hash = binascii.hexlify(hash)
        return (salt + hash).decode('ascii')

    def verify_password(self, provided):
        """Returns validity of provided password"""
        salt = self.password[:64]
        stored_password = self.password[64:]
        hash = hashlib.pbkdf2_hmac('sha512',
                                    provided.encode('utf-8'),
                                    salt.encode('ascii'),
                                    100000)
        hash = binascii.hexlify(hash).decode('ascii')
        return hash == stored_password

    def add_to_challenge(self, primary_key):
        """Add self to the challenge with specified primary key"""
        c = apps.get_model('user_management', 'Challange')
        try:
            challenge = c.objects.get(pk=primary_key)
        except c.DoesNotExist:
            return 1
        if self in challenge.users:
            return 2
        else:
            challenge.users.add(self)
            return 0

    def check_cookie(self, token):
        """Checks if cookie matches stored token in database"""
        return self.token == token


class Steps(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateField()
    step_count = models.IntegerField()


class Calories(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateField()
    calories = models.IntegerField()


class Balance(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateField()
    fpoints = models.IntegerField()
    currency = models.IntegerField()
    wins = models.IntegerField()
    matches = models.IntegerField()


class Challenge(models.Model):
    """Types of competitions    |   Competition uses                     |   Prizes     |   Use case
       1: step-count            |   1. 1v1, 2v2, 3v3, 4v4, 5v5           |   1. Free    |   1. Private
       2: weight-loss           |   2. Leaderboard of xx amount of days  |   2. Paid    |   2. Business
    """
    name = models.CharField(max_length=78)
    access_key = models.CharField(max_length=8, default=None)
    type = models.IntegerField()
    competition = models.IntegerField()
    max_players = models.IntegerField()
    start_date = models.DateField()
    end_date = models.DateField()
    price_type = models.IntegerField(null=True)
    price = models.IntegerField(default=0)  # In euro cents
    users = models.ManyToManyField(User, related_name='user')

    def is_private(self):
        """Returns if the challenge is private"""
        return self.access_key

    def get_users(self):
        """Returns a list of all primary keys of users that participate in the challenge"""
        return [user.pk for user in self.users.all()]


def get_image_path(instance, filename):
    return os.path.join('static/images/qotd/', str(instance.id), filename)


class Quote(models.Model):
    quote = models.CharField(max_length=368)
    name = models.CharField(max_length=40)
    location = models.CharField(max_length=40)
    day = models.DateField()
    image = models.ImageField(upload_to=get_image_path, blank=True, null=True)
