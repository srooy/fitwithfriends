from django.shortcuts import render, HttpResponseRedirect
from django.apps import apps

# Create your views here.
def authenticated(data):
    email = data.get('emailFF')
    token = data.get('tokenFF')
    user = apps.get_model('user_management', 'User')
    try:
        user = user.objects.get(email=email)
    except user.DoesNotExist:
        return False
    return token == user.token


def index(request):
    if not authenticated(request.COOKIES): return HttpResponseRedirect('/login')
    return render(request, 'dashboard/index.html')


def challenges(request):
    if not authenticated(request.COOKIES): return HttpResponseRedirect('/login')
    return render(request, 'dashboard/challenges.html')


def leaderboards(request):
    if not authenticated(request.COOKIES): return HttpResponseRedirect('/login')
    return render(request, 'dashboard/leaderboards.html')


def wallet(request):
    if not authenticated(request.COOKIES): return HttpResponseRedirect('/login')
    return render(request, 'dashboard/wallet.html')


def settings(request):
    if not authenticated(request.COOKIES): return HttpResponseRedirect('/login')
    return render(request, 'dashboard/settings.html')
