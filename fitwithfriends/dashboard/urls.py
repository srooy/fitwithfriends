from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index),
    path('challenges/', views.challenges),
    path('leaderboards/', views.leaderboards),
    path('wallet/', views.wallet),
    path('settings/', views.settings)
]