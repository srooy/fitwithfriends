from django.shortcuts import render
from django.http import HttpResponseForbidden, HttpResponse
import json, secrets
from django.apps import apps


# Create your views here.
def auth(method):
    if method != 'POST':
        return False
    return True


def wrong_login():
    data = json.dumps({
        'error': 'email or password is incorrect'
    })
    response = HttpResponse(data)
    response.status_code = 400
    return response


def login(request):
    if not auth(request.method): return HttpResponseForbidden()
    email = request.POST.get('email')
    password = request.POST.get('password')
    user = apps.get_model('user_management', 'User')
    try:
        user = user.objects.get(email=email)
    except user.DoesNotExist:
        return wrong_login()
    if not user.verify_password(password): return wrong_login()
    token = secrets.token_urlsafe(97)
    user.token = token
    user.save()
    data = json.dumps({
        'token': token
    })
    response = HttpResponse(data)
    response.status_code = 200
    return response


def register(request):
    if not auth(request.method): return HttpResponseForbidden()
    email = request.POST.get('email')
    password = request.POST.get('password')
    fname = request.POST.get('fname')
    lname = request.POST.get('lname')
    gender = bool(request.POST.get('gender'))
    user = apps.get_model('user_management', 'User')
    try:
        user.objects.get(email=email)
        data = json.dumps({
            'error': 'an account with this email address already exists'
        })
        response = HttpResponse(data)
        response.status_code = 400
        return response
    except user.DoesNotExist:
        pass

    password = user.create_password(password)
    token = secrets.token_urlsafe(97)
    user.objects.create(fname=fname, lname=lname, email=email, password=password, token=token, gender=gender)
    data = json.dumps({
        'token': token,
        'email': email
    })
    response = HttpResponse(data)
    response.status_code = 200
    return response


def authenticated(data):
    if data.method != 'POST' or \
        'emailFF' not in data.COOKIES.keys() or \
        'tokenFF' not in data.COOKIES.keys(): return False
    email = data.COOKIES.get('emailFF')
    token = data.COOKIES.get('tokenFF')
    user = apps.get_model('user_management', 'User')
    try:
        user = user.objects.get(email=email)
    except user.DoesNotExist:
        return False
    return token == user.token


def create_challenge(request):
    response = HttpResponse()
    response.status_code = 403
    if not authenticated(request): return response

    challenge = apps.get_model('user_management', 'Challenge')

    name = request.POST.get('name')
    try:
        challenge.objects.get(name=name)
        data = json.dumps({
            'error': 'challenge with this name already exists'
        })
        return HttpResponse(data)
    except challenge.DoesNotExist:
        pass
    input_type = 1 if request.POST.get('type').lower() == "step count" else 2
    startdate = request.POST.get('startdate')
    year = startdate[6:10]
    month = startdate[0:2]
    day = startdate[3:5]
    startdate = year + '-' + month + '-' + day

    players = request.POST.get('players')
    keys = {
        '1v1': 1,
        '2v2': 2,
        '3v3': 3,
        '4v5': 4,
        '5v5': 5,
    }

    comp = keys.get(request.POST.get('comp'))
    enddate = request.POST.get('enddate')
    year = enddate[6:10]
    month = enddate[0:2]
    day = enddate[3:5]
    enddate = year + '-' + month + '-' + day
    pricetype = request.POST.get('pricetype')
    price = request.POST.get('price')
    key = request.POST.get('key')

    challenge = challenge.objects.create(
        name=name,
        access_key=key,
        type=input_type,
        competition=comp,
        max_players=players,
        start_date=startdate,
        price_type=1 if pricetype == 'Currency' else 2,
        end_date=enddate,
        price=price,
    )
    email = request.COOKIES.get('emailFF')
    user = apps.get_model('user_management', 'User').objects.get(email=email)
    challenge.users.add(user)
    challenge.save()
    return HttpResponse(200)
